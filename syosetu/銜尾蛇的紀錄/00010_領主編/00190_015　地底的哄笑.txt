鮮血在黑暗的坑道之中飛舞。

血，肉，骨。被煤油提燈昏暗的光照亮的風景「原文就是風景，但是想不到什麼好的翻譯，用『四周』或者『周圍』更好嗎？」，瞬間染上了怪異的顏色。
儘管心情有些悲傷，但是那裡沒有哀鳴。如果說原因的話，那是因為殺戮進行的太快了。作為那個的替代，響遍了進行蹂躪那一方的哄笑聲

「kukukuhaahahaha⋯⋯！！脆弱！脆弱！太脆弱了！什麼反應都沒有！這種程度我早就習慣了，就僅僅這樣根本不算什麼！」

發出聲音的人影，是穿著上下成套的禮服和像血一樣紅色的圍巾，然後身上披著黑色的披風，貴族風的青年。雖然有著和黑暗潮濕的地下不相配的瀟灑「帥氣？」，但是有一個異常引人注目的部分。

是手。那雙手的指甲異樣地伸長，彎曲，呈現出簡直是殘酷的拷問處刑器具的樣子。
那個奇怪的爪將他所具有的殘虐性直截了當的表示了出來。那是虐殺這個黑暗世界原始居民的凶器。

『pugiiiiiiiii！？』
『gyagyagyagya！？』

因為過於驚訝，連生存本能都被凍結了嗎，驚訝遲疑了一會兒後存活下來的才發出悲鳴。
就像聲音聽到的那樣，那都不是人類。

作為群體的統率者的橡樹，及作為僕人的哥布林數體。不管哪個都是把洞穴作為主要的住所的怪物們。
個體只有一般孩子大小唯一優點小狡猾的哥布林姑且不論，橡樹也持有普通的成年人敵不過的審身體能力。但是，只是那個。在某種程度上稍微有些超出常人的臂力，是作為殺戮主演的青年的對手無法匹敵的。
青年是不是也考慮那個，收斂起笑容之後馬上變成憂郁的表情。

「⋯⋯無聊。好無聊啊。果然這種程度的東西很無趣」

一邊說著，一邊降沾染了血的異形的爪放到嘴邊，用舌頭舔了起來──然後馬上吐了出來。

「不好吃。好弱，而且無趣，血的味道也不好。啊啊，你們為什麼還活著？這樣的存在明明連一點價值都沒有。」

青年的臉不愉快的扭曲著，一邊輕揮著手臂。
接下來怎樣呢。作為凶器的指甲眨眼間縮了回去，那雙手又恢復了之前優雅的形態。
看見對方收回了武器，怪物們放心的吐了口氣。
但是，

「⋯⋯你們對於我的爪子一點價值都沒有。所以，快點去死。死亡之視」

被陸續放出的，是無情的死之宣判。
那是攻擊視野所到之處全部敵人的即死的詛咒。被像凍僵一樣的死之手臂入侵，很快地掌握了魔物們的心臟。

『⋯⋯！？』

再一次的大規模殺戮，果然沒有涌起哀鳴聲。
注意到的時候，所有的怪物都想斷了線的人偶一樣倒在了地上。
滿足的凝視著那個場面，青年再次浮出笑容。
並且，突然的變成看瘋狂的大笑。

「Kua，fufufu⋯⋯這樣的高等咒文，無詠唱⋯⋯，！我果然很厲害啊 這個新的身體！太棒了已經停止不住笑了！！」
「也請勉強止住。洞穴裡有回聲所以非常吵。」

我忍不住從後面打招呼。
於是他──夏爾・法蘭茲・修密特，伴隨著Pitari的聲音瞬間下跪

「啊，是⋯⋯對不起，主人。之後得意忘形了，是⋯⋯十分抱歉」

縱使直到剛才為止狀態看起來很好，這個變化吧。我忍不住用手去按太陽穴

⋯⋯情緒的落差激烈過頭了，夏爾啊。

───

我們現在，正在前往瑪爾蘭郡的迷宮之一『舊Ruiru礦山』

夏爾的準備活動與被荒廢礦脈的盈利性的確認，另外一有機會農民就提出來驅除魔物的急切要求
因此目的之一是測試夏爾的力量，但是

「雖然在吸血鬼陶醉在血中時不應該說，但有時動作太多了。」

──杜耶說。

「有對那種程度的雜魚射出即死魔法的笨蛋嗎？這是對魔力的浪費」

──朵萊依說。

「對⋯⋯那種程度的智力、實力的怪物，根本不需要說話。」

──優妮說。

來自前輩的糟糕的評價。

「是，是⋯⋯萬分抱歉前輩們，是⋯⋯」

又一個勁兒地道歉的吸血鬼領主。
正座在礦山的地面上連續不斷的謝罪的高位吸血鬼之類的，有史以來他不是第一次嗎？是某種意義上的壯舉。
看見這樣蜷縮起來乞求的樣子，前幾天優妮對我說的處理不是什麼杞人憂天嗎，這樣想著。

⋯⋯不不，疏忽大意是禁忌。即使他原來也是學院的降靈學科當時進入前五的優秀人才。如今又是邪惡的吸血鬼。這樣可怜的身姿也是為了招來周圍人輕視的把戲的可能性也不是零。雖說是有相應的對策，但他背叛的威脅實際上相當高。就像前世的世界裡的臥薪嘗膽，也有韓信受胯下之辱的典故雖說今天的表現令人失望，但是如果沒有拔掉背叛的獠牙將來的威脅度仍不會有變化。今後也慎重處理吧。

暫且，為了不要產生多餘的反感，稍微加入一些服從嗎？
在這樣的打算的思考下，我開口了。

「夏爾，不用那麼惶恐也可以。這次沒能做成的事，在下次讓他成功就好。而且你實際上是我的共同研究者吧？非專業的戰鬥被指摘出一點問題，並沒有屈服的必要。嚯啦，就算不像樣也是我的『作品』啊」
「噢，主人！主人，歐布尼爾大人是寬宏大量的！我非常的感激連眼淚也流出來！」

被安慰之後非常有氣勢的站了起來，一直著自己的肩膀做出忸怩的樣子。到底到哪裡為止是真的他呢？
Maa，Ii。就算考慮也沒有用那就乾脆不去想了。

「比起那個，作為首要目的的夏爾的練習結束了。到下一個地方去吧，朵萊依。」
「遵命」

受到指示，朵萊依立刻開始著手準備工作。第二個目的是，為了對礦脈重新進行徹底的調查。

「≪大地的精靈呦，解答我的疑惑吧。汝隱藏的奧秘是什麼。汝擁有的財富在哪裡──範圍偵查≫！」

在詠唱的同時，魔力從朵萊依體內有條理的涌出，變成術式向周圍擴散開。這是借助了土屬性精靈的力量構造的，廣域偵測魔法。作為黑暗精靈的朵萊依特有的大量魔力和極高的精靈親和性，那個效果疊加，是探索的範圍和精度大幅度的提高。不光是這個廢棄礦山的內部，就算是周圍地下的資源也應該全部都能找出。

果然，沉默了數十秒後，她抬起了頭。

「⋯⋯這裡的礦床不行。能挖到的地方附近的資源已經開採光了。」
「果然啊。如果稍微有一點利潤可圖的話，那些貪婪的原代理官們是不可能撒手的。」

正因為那樣，才會丟棄給怪物成為他們的巢穴。Maa，雖然在最開始就沒有什麼期待。

「但是──請感到喜悅，主人。從這裡向東北方15km的地方有大規模的銅礦脈的樣子」
「噢，那可真是厲害！」

銅礦山，並且可以大規模的生產的話，那瑪爾蘭的財政也能極大的闊綽起來。即使作為錬金術師的我，可以加工成各種各樣材質的銅，變得能夠大量獲得也是值得高興的。

但是

「⋯⋯從這裡向東北方15km的話，是險峻的山岳地帶。途中的森林也非常茂密，沒有舖設道路的可能」
「啊！」

因為優妮發言，發現自己是空歡喜。
就是啊，這裡，本來不就是在深山裡嗎？從這裡更加深入的話，到底會成為怎樣的秘境呢？

無論多少有潛力的礦脈，在礦石的採掘，搬運，運輸，流通上，首先如果沒有到哪裡的道路的話就不可能實現。向哪裡舖設道路要多少錢呢？雖然那不是我所熟悉的領域，不過，那個方法沒有可能這件事也是明白的。至少憑現在我領地內的財政，根本沒有負擔那個開支的能力。

「⋯⋯非常抱歉，主人。如果無益的情報讓您心情變差了的話。」
「啊，不。反正發展瑪爾蘭的話，可以向那裡建造道路的時代也會到來吧。現在預先記住一點損失都沒有」

好的話在幾十年，二十年之前，壊的話說不定成為百年之後的話了。

「啊，可是如果主人製造格雷姆前往的話，不是馬上就能進行挖掘嗎？」

杜耶插嘴。
但是在我回答他之前，夏爾誇張的嘆了口氣。

「yareyare，你在說什麼呢？砍倒森林，挖開山，運送開採出來的銅⋯⋯那樣能大規模模仿著做出這些的格雷姆，是已經超出國家容許範圍的超兵器喲？與為了灌溉工程那種程度的工作使用的所謂的泥漿格雷姆是另外一回事。被目擊到了最後，會傳來來自皇宮的傳喚啊。」

他咧著嘴笑著。是向對剛才的戰鬥被指摘出來的報復吧。受到了那個的杜耶的臉抽搐起來。

「話雖如此可是呦⋯⋯對你的指摘為什麼會那麼生氣啊！？」
「噫！？Bo，暴力反對！」

朵萊依反射性地向藏在背後的夏爾說「別碰屁股」同時（夏爾）被踢飛進了坑道咕嚕咕嚕地滾了下去。

不知何時開始的小故事先放一邊，大體就如夏爾所說的那樣。如果製作那種東西，會造成目擊的附近居民的恐慌，有誰會向冒険者公會通報，在最後那些傳聞會從那裡傳到國家。用錬金術在瑪爾蘭的很多地方做出貢獻的我，但是，卻做出了到現在為止仍被理解為超出神格化的大事件。

如果那樣我和我的『作品』們一起採掘呢？也會被看做那樣，這也是一部壊棋。之前做到土地灌溉的改良那種程度的，稍微有能力的錬金術師，哪個領主都能隱藏起來，但是那與在深山裡開發礦山的難易度是不同的。如果知道有人可以以少數人完成那個的話，會被中央的傢伙們及周圍的諸侯高度警戒。本來我的評價就不好，而且是被兄長及中央集權派盯上的立場。這個以上多餘的麻煩事絶對不幹。

「Maa，只能在礦山附近踏實穩重的探索。不然就沒有其他辦法了。」
「是。而且主人的本意，是為了進行不老不死的研究」

和優妮說的一樣，對我來說經營領地是很好的研究資金的來源，說得難聽點不過是多餘的彎路罷了。而且拘泥於那個會導致本末倒置。總之最近被拉瓦萊侯爵盯上了，維克托爾達を送りこまれたのは，嚴重的失誤也存在。要好好反省。

「那麼，大部分的事情都結束了，最後做完收尾回去吧？喂，夏爾！拜託再工作一下！」
「Ha，是 ｉ⋯⋯」

被叫到搖搖晃晃靠近的夏爾，因為在處滾動弄得滿是泥。與其說是吸血鬼領主，不如說是剛剛從土裡復甦的僵屍。
可是，儘管有作為死靈術師的能力，但不死化的例子大陸上也是屈指可數。恐怕用三根手指數都有餘裕。

Maa，從這裡開始的工作就要極大的浪費那個才能。

「那麼，開始設置吧。」
「是～。⋯⋯確實高級骷髏兵人偶有五體，很好了吧？這種程度，無詠唱可是很有餘裕的」

說著，斗篷下的右手手指突然發出響聲。

──瞬間，急劇的變化出現了。

他在坑道中屠淨的怪物的死屍，開始聚集到一個地方。並且因為超自然的力量被壓縮，在那個過程中失去了物體的形狀，變成了漆黑的粒子。這是被稱為瘴氣的負的能量體。是對一般生命體有害的東西，不過，對於擅長暗黑系統的魔法的術者和黑暗中的怪物而言，操縱這個能引起各種各樣的現象。

「啊⋯⋯黑暗的同胞，我的眷屬！侍奉這高潔的身軀，竭盡你的堅強與忠誠吧！≪創造・骸骨≫！！」

作為不死族的夏爾接觸到瘴氣開始變得恍惚，開始一邊露出可疑的笑一邊繼續施放術。那個身姿，確實適合作為不死者之王的吸血鬼。

只是──

「喂，這傢伙不是說能夠無詠唱嗎」
「⋯⋯我記得，≪創造・骸骨≫的詠唱不是那樣的東西。」
「恐怕是即興詠唱。我的評價先保留」

──好像，因為他的樣子才沒有遭到吐槽。

在這個過程中，瘴氣散開了，這是夏爾施放的術成功的表現
總覺得稍微有些青色的骨骼淒慘的暴露在外面，拿著不知從哪裡出現粗糙的劍和盾的魔物。這就是骷髏兵，那個也是被稱為高級骷髏兵的上位種。那個正好五體，像我希望的那樣出現了。

「不可思議。從哥布林和豬頭人的屍體中生成的，但怎麼看都是人的骨骼。」
「那是當然！不是死者生前寄宿的靈魂變成不死者的樣子，而且是以屍體為素材，短暫的賦予了生命，做出了新的不死者。成為我這樣的術者的話，改變骨骼樣子什麼的是超簡單的喲！」
「真是有一點小事情緒就會起伏的男人，這傢伙⋯⋯」

向雀躍的夏爾投向驚訝的目光的『作品』們
到這裡為止才是他本來的性格嗎？這樣想著。在學院的時候總是戰戰兢兢的，和現在一樣被恐嚇命令就會蜷縮起來。只是以前不知道有口吃這回事，其實情緒突然高漲這種事也是有過的。

「嚯啦嚯啦，不到最後不要放鬆啊。命令的設置也拜託給你了吧？」
「啊是─！小事一樁─！」

雖然只是業餘愛好，但是因為能炫耀自己擅長的領域吧，他他的心情太過好了。如果是工作的話心情愉快會比較好吧，那就先放一邊吧。

「知道了嗎，你們？『侵入到這個洞窟的人，引誘到裡面然後殺死』。『逃脫者不追就好』。然後──」
「像這樣在洞穴裡設置高等骷髏兵，是為了驅除哥布林之類的田地裡的怪物嗎？」

就是如此。本來這應該是冒険者的工作，不過不巧的是瑪爾蘭這地方不僅交通不便而且油水很少。中堅以上的冒険者沒有理由來，新人冒険者本來就沒有旅費。

首先公會的分部正在被放置，在人比較稀少的瑪爾蘭郡內只有一家，而且生意蕭條。主要的工作是書信的收發，已經是一副自稱優秀的郵局的狼狽相。不用說，也沒有接受討伐委託的冒険者。所以，如果想在瑪爾蘭郡治退怪物，要特意向周圍郡的公會拜託嗎，要領民自己治退嗎，領主──總之就是我──只能親手討伐。

不過，第一是會花費多餘的錢。第二是自衛團就這樣反抗組織發展有可能會影響治安。最後第三就是對我們來說會很麻煩。不，我們並非懶惰。因為這附近的怪物很低級，到處轉還掉落不了像樣的素材。屬下的武官們，馬虎的發起兵糧飼草的採購及武器的護理等，也會產生各種各樣麻煩的費用。

因此，決定在那個作為怪物聚居地的洞窟裡，配置驅除用的使魔。這次決定長時間設置，並且自動捕殺原野裡的怪物。還有，使魔的水平控制在這種程度就可以，萬一被領民及好奇的冒険者發現，那樣引起騷亂可不行。

我也可以用錬金術適當的操作使魔，不過，但那果然只會成為發出錬金術師氣味的東西。因為將我和洞穴裡冒出的怪物聯繫起來的人，早晚會出現的。最有可能的是，隱藏名字的弟弟對手是沒有大人樣的某伯爵啦，年紀大了生下私生子的某伯爵拉。

就是那樣，所以拜託即使地道的死靈術師也是不死者的夏爾，通過模仿自然產生的控制成功製作出高級骷髏兵。當然，優妮和朵萊依用《創造・骸骨》也能盡可能的製作，但是那樣我也可以。不過，哥布林姑且不論能戰勝的豬頭人的高級骷髏兵製作起來非常難。還有，果然不擅長靈魂的操作因此命令的準確度也會下降，材料也需要真正的人類屍體。這個不是作為魔導師的實力問題，而是死靈魔術的適應性問題。

哎？屍體的材料很多都在實驗室裡吧？玩笑先停下。萬一實驗的痕跡泄露到了外部呢，那麼不是不能那麼做了嗎？到現在實驗的痕跡也不難看出來，不過我的主張是凡事小都要慎重起見。居住在王都的時候，除了實驗產生的貴重的屍體樣本之外，全都好好用火葬燒到骨頭都碎掉。⋯⋯話雖如此，因為搬到瑪爾蘭的時候無法搬運，變成了樣本也要燒掉的窘況。

不久，夏爾的工作也好像結束了。

「──好，這樣就結束了。命令的設置結束了，歐布尼爾君」
「辛苦了。那就這樣回去吧。」

在我說話的同時，全員向朵萊依周圍聚集。用空間轉移魔法≪高等傳送≫瞬間移動，一瞬間回到了住慣了的房子裡。

「真是方便啊，轉移魔法。托你的福能從冬季的天空下跳躍到據點裡」

杜耶感慨的說。就是如此，季節是還冬天。因為夏爾是剛剛完成的，所以我想明白的人應該是知道的。

「那個部分，魔力吞噬失敗的話會直接進入牆內。如何離開？到離我遠的地方容易遇到轉移事故。⋯⋯主人，請將您的手給我」

承著這話，拿起她的手。另一邊的手由杜耶握著。優妮和夏爾只能勁靠在附近。萬一這二人產生偏差，自己承擔術式的干涉調節出現的位置也能防御事故。對我來說，魔力的控制技術姑且不論主要的問題是對轉移的干涉是很難的。

朵萊依結束了期望萬全的冗長的詠唱，閉上眼詠唱最後的魔法的名稱

「⋯⋯≪高等傳送≫！」

扭彎天意，越過次元之壁，我們離開廢坑道。被留下來的高級骷髏兵只要還在工作，暫時就沒有來這裡的機會。

「原來如此，⋯⋯這麼說來，現階段著手採掘的礦脈範圍相當小啊。」

朵萊依將探查得到的情報在原本的地圖上用紅筆修改完後，維克托爾抬起了頭。他正在用他的實力和門第Ｈ慢慢佔據內政管筆頭的地位。Maa，前輩們的地位完全被削弱了，而且一開始開始沒有這麼能幹。意想不到，說起來如果他一開始有實力的話，應該也沒有這種程度的麻煩吧。

「預想以上的豐富的礦產儲備，不過由於成本和技術的關係很遺憾能夠實際用到的東西出乎意料地少」
「完全如此啊。說起來，就算全部能挖掘，到處都是毒氣也是很麻煩的事。集中起來反而比較好。」

我坐在辦公室的椅子上輕鬆的說雖說大部分工作都交由別人做著，但是重要事項的決策以及關於錬金術的領域就不得不我來做了。特別是這是作為貴重的素材來源的礦山。這種事不認真做的話，之後也會成為麻煩事。雖說在用腦手術來防止背叛，但是牢騷這種程度是可以說的。

「有一定的道理。Maa，鑑於目前的財政狀況，也沒有那樣富餘的資金投入到礦山裡」
「所以，從你來看那種最理想？」
「這裡是銅，錫，鉛的礦脈重疊的地方。山裡也存在金和銀，這樣的東西就算挖掘到了國家也會恬不知恥的跑來恐嚇，這樣的事情也是有過的」

唔，總是不能簡單點嗎？這麼說來，金山和銀山可是作為封建君主直轄化的代名詞的東西。

「殘念⋯⋯金和銀自己也能挖掘的話，高位禮裝也可以隨便做的」
「領主閣下」
「啊，嗯。我知道的我知道，好好地聽著。推薦那個三重礦脈？這之後要對應對礦毒的設施預先進行估價、工作的分擔以及協商。」

因出乎意料地敏感的反應而驚慌地回答。不管是維克托爾也好的魯貝爾也好，我對我的伙伴意外的縱容。把那部份的工作也給他，這樣這種時候應該有點聽話了吧，之類的，這樣想著。

「拜託了，真的。礦毒問題是很嚴重的⋯⋯那麼，下面的議案。是關於委託給魯貝爾們的正規的宅邸建築，但是」
「果然我希望的土地不行是嗎？」

試著搶先回答。
正巧那時提案作為領主宅邸預定建築的地方。很適合設置我錬金術的實驗室，土地的靈力很強所以指定在那裡。

但是，對於那個有幾個問題⋯⋯，

「一定是不行的吧。被指定的土地裡面，大部分都太過遠離城市了。無論如何，也不能作為行政中心來使用。城市中的東西不談。沒有建造教堂的地方呢。為了宅邸而損壊教會什麼的，本來就低的評價這次真的會降到最低吧？」

維克托爾的回答和預想的一樣。

在城市內靈力強的土地，一般會被教會扣留下來。是為了作為聖職者使用大規模的神聖魔法的儀式場。如果強行奪走，會變成在本來就多的敵人裡再加上宗教的困境。而且，在這個世界的教會由於能使用回復魔法所以也有著醫院的作用。如果作為領主的我那樣任性的破壊教會的話，來自領民的支持降低也是必然的吧。

話說回來在偏僻的地方建造宅邸導致行政處理能力降低的話，在王都的同伴的缺點也會傳開。真是令人為難的事情。

「那麼乾脆點，把實驗室和宅邸分開呢？」
「如果做得出來的話就不用這麼辛苦了。但是，全部都要考慮啊。領主每天，從遠離城市的神秘設施出入的景象。如果兄長和你的父親聽到這些，肯定會歡喜的著手調查。本來就來這裡之後研究過激化」
「那是自作自受吧？那個暗精靈的奴隷還好，連吸血鬼都拉進來了。如果我沒被洗腦的話，教會啊高等法院，一定會衝進冒険者公會吧」

明明是主人在吐露著煩惱，這個家臣卻在說這種話
我禁不住仰望天。

「啊，真是的⋯⋯想著從昏暗的地下迷宮中歸來的話，現在這次。完全沒有空閑的心情──」

那時。我腦海裏有閃現東西。

──那麼乾脆點，把實驗室和宅邸分開呢？──

──想從昏暗的地下迷宮中歸來的話──

──金和銀自己也能挖掘的話，高位禮裝也可以隨便做的──

──如果製造大型格雷姆前往的話，馬上就能──

──真是方便啊，轉移魔法 ──

幾句話變成了點，在哪之間被線連了起來，編織出一個完整的圖。
這個⋯⋯說不定是個很棒的主意？
不如說，這樣簡單的事為什麼沒發現呢，我啊。

「閣下？怎樣了，閣下？」
「──是這樣啊，嗯。不要緊嗎，如果這樣做？萬一有人對館進行調查，用這個方法應該沒有問題⋯⋯」

把維克托爾的聲音當耳旁風，我著手對那個方案進行總結。
然後估算完成後從椅子上起身呼喚她。

「優妮」
「是，就在您身旁。」

無聲無息出現的優妮讓維克托爾瞪大眼睛，無視。
現在不是這種時候，我如果叫她她就會快跑過來之類的，在這裡是常識。

「把新的實驗室的建設計劃，提前執行吧。不要慢慢悠悠的等到雪融化的時候。從現在就開始著手」
「遵命。」
「請，請您等一下，閣下。從現在開始就建設實驗室嗎？地址呢？而且新的居館在建哪裡？」

總覺得維克托爾很吵啊。但是啊，他要是聽了這個點子，一定會沉默著遵從吧。

「關於新居館的計劃，拜託你和魯貝爾了。為了在好的場所好好的建造。要是你們，一定會建造在貴族看來不那麼可笑的東西吧？」
「那當然，不過⋯⋯」
「但是，請擴大製作地下的房間。名義用庫房啊紅酒酒窖啊不管什麼都好」
「因為實驗室？」
「就想你提案的那樣，決定和宅邸分開建造。地方⋯⋯在哪裡」

一瞬間的考慮之後，用手指在地圖上一點。

「就在這裡。決定了」

看我指定的地點，維克托爾一點也沒有秀麗的貴公子的樣子張開了口

因為那裡不是我列出來的建設地點吧。不但過於遠離城市，而且不管怎麼考慮都不是適合建設實驗室的布局。
但是，那就是我這個點子的得意之處。

「吶，是在考慮什麼吧閣下！？閣下！？」

隔了好久從背後聽到他混亂著急的聲音，我和優妮已經從辦公室出來了。
好像不是，心情雀躍的踏著愉快的腳步。
廊下で擦れ違った家臣の一人が不氣味がっていたが、なに氣にすることは無い。

「主人看上去開心比什麼都好。因此，有怎樣的主意？」
「嗯，聽我說」

對優雅地尋問的優妮，我毫不隱瞞地說出剛剛想出的點子。
聽那個的她啪嗒啪嗒的眨著眼睛。

「去考慮了驚人的事情啊⋯⋯」
「反對嗎？」
「不沒──有那個理由」

優妮好像也感到吃驚，不過馬上就做出了肯定的回答。
像對夏爾那時候一樣地，一點錯誤都不放過的她那樣說。我越發對這個想法越發有了自信。