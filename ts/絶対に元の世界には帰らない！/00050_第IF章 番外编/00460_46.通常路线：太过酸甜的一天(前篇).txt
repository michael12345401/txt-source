這路線是打倒魔王後，只有光彌回到原本世界，之後也沒有再回來異世界的路線。也就是陰矢＆影耶完全勝利的一天。
只是，影耶還是保持被艾伊西亞討厭的關係，魔王也沒有完全毀滅變成了封印狀態。嘛，和本話沒關係就是了。作者也沒有考慮的太多，請好好享受影耶等人一如往常的日常吧。


════════════════════

老樣子的地下室。
夏緹亞一邊吃著餅乾一邊悠閑地看書。我停下作業中的手，緩緩地拿出特伊西亞斯，發動改造魔法。

「⋯⋯《自我改造》」

紫電閃爍，我的身體變化成影耶的樣子。
一邊注意不要踩到變得寬鬆的衣服，我一邊走向鏡子前。

「咦，突然怎麼了，小影耶？」
「⋯⋯」

無視夏緹亞的話語，我靜靜地注視鏡子。
面對映出全身的鏡子，我從頭到腳，沒有放過任何一處地掃視影耶的身體。

「真是的，回答一下唷，不然媽媽要鬧別扭了喔～」

夏緹亞從後面緊緊地抱上來。
和我緊緊相依，擺出欸嘿嘿滿足笑臉的夏緹亞。
抱住體現我理想的美少女，一個人獨佔的夏緹亞。

透過鏡子看見這場景的我，讓一直以來沉積的感情爆發出來。

「⋯⋯⋯⋯滑」
「嗯？」
「只有母親可以這樣太狡猾了！！！」

對著突然生氣的我，夏緹亞一臉困惑地問道。

「⋯⋯那個，狡猾說的是？」

我離開夏緹亞的擁抱，用手指著自己大聲叫了出來。

「明明⋯⋯明明這樣的美少女就在這裡而已，我──我卻連一次都沒法見到！然而夏緹亞卻可以整天親熱⋯⋯！」
「不是，就算你這樣說⋯⋯」
「這種情形我已經受夠了！我也想和影耶做各種事情的呀！像是約會像是牽手，還、還有親吻之類的！」
「慾望太純潔了」

我維持著影耶的樣子撲入床上，抱緊枕頭。
我一邊滾來滾去，一邊碎念起壓抑不住的怨念。

「嗚嗚，已經不行了⋯⋯透過照片還是鏡子已經滿足不了我了⋯⋯好想直接見面親親熱熱⋯⋯」
「可是你這就算說見面也⋯⋯⋯那個，不然就我來給你做些戀人般的事情？」
「夏緹亞太笨了不要⋯⋯人家只想要影耶⋯⋯」
「區、區區童貞還如此猖狂⋯⋯！」

夏緹亞好像生氣地說著什麼，不過現在的我完全沒有在意的心思。

夏緹亞確實是個美人，性格也和我十分合得來，還意外地有些溫柔的地方。不過因為對夏緹亞本人說這些會讓她得意忘形過頭，所以我絶不會說出口就是了。

不過就算這樣，我還是想和影耶親熱。想和濃縮所有我喜好要素的完美美少女來一場酸酸甜甜的戀愛。

「嗯～⋯⋯既然這樣，我也有個想法」
「欸？」
「用我的改變魔法只取出肉體的構成要素，再用魔力做出暫時的肉體。雖然沒有靈魂，但依照命令動作的話還是辦得到的。」
「真的嗎⋯⋯！？」
「只是這樣花費大量魔力就是了，要試試看嗎？」
「當然！」

我從床上坐起，轉身面向夏緹亞。
真不愧是睿智的魔人。剛才的笨蛋發言我就撤回吧。

「好了，快點快點！」
「欸～這術式相當複雜，而且我也是第一次這樣使用，所以很耗集中力⋯⋯」
「讓你吃物品箱裡面的豪華冰淇淋芭菲也可以唷」
「術式啟動──《他者改變・幻想分靈構築》」

夏緹亞從我體內取出魔力，青雷與紫電交織閃耀著。

消耗大量魔力的疲勞感，以及身體中什麼重要東西被拿走的喪失感。我撐住搖晃的身體，等待魔法的結束。

「──好了，術式結束」

光芒消失後，我旁邊躺在床上的，是全裸的黑髮──男子。

「⋯⋯⋯⋯」
「⋯⋯⋯⋯」

我瞄了一眼鏡子，上面映出的是和剛才一樣的影耶身體。

「⋯⋯笨蛋！」
「沒、沒辦法吧！畢竟我也是第一次用！」
「吵死了，害我空歡喜一場⋯⋯！冰淇淋芭菲不給妳了！」
「為什麼唷，我也不是故意的！」

在我們吵鬧的時候──背後的什麼起來了。

「咕⋯⋯發生了什麼⋯⋯？奇怪，為什麼影耶⋯⋯！？」
「欸？」

※

根據夏緹亞的說法，這似乎不是靈魂寄宿上去，而是混入我的靈魂碎片，做出的擬似精神而已。

穿上備用衣物的分身，對著夏緹亞的話點了點頭。

「啊啊，確實⋯⋯總有種被從本體分開的感覺」
「原來如此，有自己不是本體的自覺嗎」

眼前有著和自己（陰矢）一模一樣的男人，用著和我一樣的語調在和夏緹亞講話。分身似乎對現狀沒有太大違和感，但我個人倒是相當複雜。

「嘛，魔力沒了我想就會消失了。只是今天能消費完嗎」
「欸，這樣沒問題嗎？分身的意識不會死掉嗎⋯⋯」

就算說不是真的有靈魂，但總還是讓人有點疙瘩。

「不會唷，結束後應該會變得像是融合回到本體一樣。記憶之類的應該也會統一在一起。」
「喔喔，原來如──」

不對，等一下唷⋯⋯？

在我思考的時候，分身和夏緹亞繼續對話。

「我這邊有分身的自覺⋯所以消失也不覺得怎樣⋯⋯不過記憶被統一的本體應該會混亂，所以還是早點變回去比較好吧？有著自己以外的自己感覺也不太舒服⋯⋯」
「嗯～說的也是⋯⋯咦，小影耶怎麼了嗎？」

我移動到分身旁邊，猛地靠近。

「⋯⋯⋯⋯」
「欸？那個，影耶⋯⋯小姐？」

為什麼要對自己加上小姐唷分身。
雖然有些煩惱，但我下定決心──十指交纏地握住分身的手。

「什──」
「母、母親。也就是說，分身感受到『和影耶握手』的記憶，之後也會和我統一嗎？」
「嗯、嗯。我想是這樣沒錯⋯⋯難道」

我拉過分身的手，緊緊地抱進胸口。

「欸，啊──」
「決、決定了，分身（陰矢）──今天一天，就和我（影耶）盡情地親熱吧！」

※

我從後面緊緊抱住分身──陰矢的身體。
是因為是自己嗎，沒有抱住男性的那種厭惡感。不過這樣自己用胸部貼到別人身上實在是太過羞恥了。

「⋯⋯那、那個，影耶⋯⋯」
「怎、怎樣，陰矢？」
「好像，碰到了」
「碰、碰到了唷」
「喔、喔⋯⋯」

一邊扮著影耶一邊叫自己（陰矢）的名字總有種無法表達的害羞。有點羞恥的演技都已經有無數的經驗了，但這種經驗還是第一次。

看著扭扭捏捏的我們，夏緹亞大大地嘆了一口氣。

「⋯⋯你們兩個也太純情了吧。明明被我抱的時候就毫無反應」
「這可是影耶喔⋯⋯？和夏緹亞不一樣，完全就是我好球帶的美少女，做這種⋯⋯」
「啊，嗯。母親直說的話就不是我喜歡的類型。」
「雙重打擊⋯⋯」

夏緹亞變得心情低落，但我們毫不在意。

雖然想過很多想對影耶還有想讓影耶做的事情，但因為羞恥過頭所以都從腦中消失不見了。

先暫時離開，冷卻一下頭腦。

「一、一直站著也怪怪的⋯⋯先坐下吧」
「啊，嗯。我也這麼想。」

陰矢在床邊坐下。
我有些猶豫地在陰矢前面坐下，把背部靠上陰矢的身體。

「什⋯⋯！」
「那、那個，抱住也可以喔⋯⋯？」

我抓住陰矢的手腕，環繞自己的身體。

「不是，那個⋯⋯」
「不、不要猶豫了，我（陰矢）唷⋯⋯！這只有今天可以做到啊⋯⋯！」
「啊、啊啊！」

陰矢從後面抱住我（影耶）的身體。

一邊忍受夏緹亞「這倆傢伙真夠麻煩的」的視線，我們就這樣無言地度過了三分鐘左右。

雖然我只有羞恥的感覺，但陰矢那邊肯定是交雜著感動歡喜激動等各種感覺吧。

雖然羨慕到了不行，但後面也會變成我的記憶所以還能忍耐。

「那、那個，還有就是⋯⋯有什麼想要我做的嗎，陰矢？今天的話，就算是有點羞恥的事情，我也會做，的說⋯⋯」
「⋯⋯」
「咦？」
「小影耶，陰矢已經被萌死了」

我、我還真沒耐性⋯⋯！

確實要是被影耶說了這樣的話我也會變得差不多的樣子，但這可是只有今天才辦得到所以加油一點啊，我自己。

讓按住臉龐後就不會動的陰矢重新啟動，我在度詢問。

「你看，我們可是好不容易才見面的喔？應該有很多想做的事情吧？」
「太、太過突然，腦袋跟不上⋯⋯」

沒用⋯⋯雖然是自己但真的有夠沒用⋯⋯⋯

「可是，在魔力用完之前不盡情親熱一番不是很浪費嗎？母親那麼怕麻煩的人，肯定不會幫我們幾次⋯⋯」
「確實是呢⋯⋯那傢伙基本上就只會在家當廢人而已⋯⋯」
「我才不想被你們說怕麻煩還是廢人什麼的！」

因為都是自己，理所當然地對夏緹亞的見解也都完全一致。

「那、那就像剛才說的，約、約會如何」
「好、好哇！行程的話⋯⋯不管哪邊想都一樣。就隨便去些想到的地方逛逛吧」
「啊，人家也想去！」
「不行」「不行」

多次叮囑夏緹亞好好顧家，並給她豪華冰淇淋芭菲後，我們就把她趕出房間了。

好了，趕快來換衣服──

「⋯⋯」
「⋯⋯」

我和陰矢對上眼神。
不管是我（陰矢）還是我（影耶），換衣服的時候都是在這個房間換。

普通來說，這時候應該讓其中一個人出去才對⋯⋯⋯

「看、看也沒關係唷」
「可、可是⋯⋯」
「雖然很羞恥沒錯，但這可是我（影耶）的換衣實況喔！陰矢也是我，所以一定想看吧！而且之後也會變成我的記憶，所以你不好好看我也很困擾！」

雖然有些複雜，但這可是可以直接看到影耶換衣的機會。怎麼可能因為害羞，就放掉這種大好機會呢。

但是因為是我，所以不管怎樣說總感覺還是會被移開視線。
我這邊明明拚命忍著如此羞恥，記憶統一後要是不能沒有好好看清楚不是虧大了嗎。

「⋯⋯陰矢」
「怎、怎麼？」
「可、可以幫我換衣服嗎？」

我抓住陰矢的手，拉到衣服的一角。

必須像是光彌的幸運色狼一樣──不對，應該是要比那個更加深刻，更加徹底地，把影耶的內衣裝扮刻畫在眼睛裡面。

「這實在是⋯⋯」
「好了啦！之後後悔看步道可是我喔！」

我應試拉住陰矢的手，直接掀起了衣服。

「不是，你現在穿的是我的衣服，所──啊」

然後，沒有穿著胸罩的胸部露了出來。

「──呀啊啊啊！？」
「咕噗！？」

因為太過羞恥，我不禁反射性地揍飛陰矢的臉。

就因為這些有的沒的花費大量時間的我們，真正出門約會的時候已經是一個小時以後了。


════════════════════

解放條件・通常路線
・影耶的好感只有本篇的一半。
・不讓影耶參與迷宮事件
・決戰前夜敗給影耶