嗞。

什麼發出了燒熱的聲音。
像直視太陽一樣的光深深地印在眼中──為了從變得通紅的景色中逃跑而閉上眼睛後，因為熱氣吸引艾夏再次睜開了眼睛。

難以置信的是，那裡已經不是草原了。
火花啪嚓啪嚓的飛散。
不要說綠色，連岩石都溶解了，稀溜溜地熔化了的沙子和赤熱的岩石像大河一樣流淌化為了岩漿。

嗞。
聲音再次響起。

像要破壊鼻腔一樣的讓人想吐的臭味彌漫。

「⋯⋯⋯⋯嗚⋯⋯」

艾夏捂著嘴巴蹲下。
那個聲音是人肉燒熱的聲音。因為被加熱的地面、飄蕩的熱氣，肉、肺、內臟烤成軟炭的那樣的聲音。

悲鳴震耳欲聾。
尖叫，尖叫，尖叫，尖叫，尖叫──艾夏的耳朵只聽得見那個。

誰的悲鳴、悲嘆充滿了戰場。
火竜的吐息（Breath）是朝艾夏放出的。
但是，不是艾夏，周圍才是受害甚大。
燒紅、潰爛，和時間一起消失，戰場到處能看到那樣的生命。
勉勉強強可以說平安無事的，只有以克麗絲塔為中心的高位冒険者和魔道騎士團（Magic Knights）一團。不論魔物還是人，全都燒死了。

「為，什，麼⋯⋯⋯⋯為什麼⋯⋯會這樣⋯⋯」
「抱歉呢，契約者小姐──分裂體這就是極限了──」

艾夏毫髮無傷。
那首先是因為娜哈特給的防具的魔法耐性、屬性耐性極為優秀。火竜的吐息（Breath）大約一半程度只靠防具的耐性彈開了。
另一半是溫蒂妮的水膜絶熱了。
艾夏右手戴著的課金道具，替身的手鐲還沒有碎。

「啊嘞─，好奇怪呢─。沒有死啊。手下留情過頭了嗎─」

漫不經心的，高亢的念話傳開。
雖然怎麼也無法想像是極盡破壊之人的聲音，但會在這種狀況下開口的人除了這條火竜以外就不存在了吧。

「但—是總覺得有點掃興呢。因為姐姐大人說要放過，所以咱還以為有多異常（Irregular），但還是真小呢。咱還想好不容易有個爭口氣給姐姐大人看的機會呢─。啊，不過，能承受住咱的吐息（Breath）這點果然即使小也是特異點沒變呢──所以請去死咯──」

陰沉的輕聲。
那個也刺激了艾夏。
艾夏她們賭上榮譽在戰鬥。拚命以自己的信念在戰鬥。
可是，突然出現的局外人卻旁若無人地踐踏、奪走了不用失去的生命。

開什麼玩笑！
艾夏的怒氣混進了魔力，在體內循環的魔力深深地，深深地褪去顏色染上了夜色。
那個顏色簡直就像娜哈特擁有的魔力一樣，艾夏無法完全制御那個。

「為什麼⋯⋯呢────為什麼，要做這種殘忍的事⋯⋯」
「啥？你說了什麼嗎？殘忍？什麼殘忍？」

那句話完全沒有包含惡意。就像只是說出了頭腦裡想到的疑問一樣，這越發刺激了艾夏。

「為什麼，要把大家捲進來！如果目的是我的話，只瞄準我就好了──為什麼！為什麼，要這樣！」
「瞄準了哦？咱只瞄準了你。其他是餘波喲？嘛，渺小的人類很簡單就死了。然後馬上就會增加。重要的是平衡，作為掌管世界調停的觀察者的竜，在盡到職責之後，即使死個十萬二十萬人也沒關係哦？無所謂。魔物也一樣。希望你恨在這裡的不幸呢」

這就是被視為與神相同的竜的話。
艾夏不知道那是否正確。
可是，憤怒毫無辦法地涌現了。
雖然不太明白，但是憤怒涌現了。
這個，這樣的，創造出這種景色的行為是正確的，艾夏絶對不認可。

艾夏的主人擁有絶大的力量，但那個力量只被揮向敵人。分給弱者力量，也會一時興起伸出援手。渺小什么的，無所謂什麼的，那種話是絶不會說的吧。將人和魔物視為同樣的生命。尊重他們。愛護艾夏這個渺小的生命。

或許是因為如此吧。
艾夏認為不講理的火竜的暴力是無法允許的。

「開什麼玩笑！他們不是因為那種理由，因為那種無聊的理由就可以死了的人！不是可以失去的人！那不是可以玷污的戰鬥！他們是娜哈特大人選出的人，不是你可以奪走的！」

變得能對直到不久前艾夏也應該認識為神明的竜口出狂言了，艾夏心想。一半是因為氣勢。一半是因為娜哈特才是對艾夏來說的絶對吧。

「莫名其妙呢──總之，請去死呢，為了咱──」

強大的手被揮下了──可以把那個稱為手嗎。
像鈎爪一樣的銳爪一根就和艾夏的身體一樣大。
手揮下。只是那樣，艾夏就變得沒法站在那個地方了。壓倒性的體格差。那看起來像是在宣告著力量的差距。
被風壓壓倒，癱軟坐在地上的艾夏全力大叫。

「南，南瓜，南瓜！」

揮下的竜的手被兩杆槍阻止浮在空中。

「『呷嘻嘻嘻嘻，Trick Or Treat？』」

漂浮在艾夏旁邊的兩只南瓜頭的小惡魔（Pumpkin Devil）像庇護艾夏一樣擋住了。杰克南瓜燈的頭部加上披著黑斗篷的那個有著用木頭形象化的四肢，手上夾著提燈和三叉槍。
那是艾夏的裝備，南瓜褲的特殊能力。

「呋欸⋯⋯啊嘞⋯⋯涼涼的⋯⋯」

失去全部防御能力的同時作為交換，有著二十四小時一次召喚兩隻守護獸，南瓜惡魔的技能。等級五十──是擁有很高的體力和火耐性的存在。作為特質系（Unique）裝備的附帶能力是等級高得破格的召喚獸。但是，基本上因為缺點太苛刻了所以是在對人戰派不上用場的裝備。

「『呷嘻嘻嘻嘻，Trick Or Treat？』」
「啥，啥玩意，這些傢伙──」

一邊對突然出現的兩只召喚獸感到困惑，一邊那樣回答的瞬間。獰猛地歪斜的南瓜的嘴被火包裹了。

「『Trick！！』」

實際上這個召喚獸是在受到提問時交一個食品系道具內被分類為糖果的東西就不會採取敵對行動的不良品。
因為也是活動的報酬，其性能玩家眾所周知，所以實在是沒用的孩子的第一名。當然如果對手是怪物的話能利用，但那是也會被一部分智能值高的敵人給糖果收買的令人憐憫的我方龍套。

但是，在異世界不可能有理解地球的文化的人。艾夏也不知道。儘管如此兩隻南瓜惡魔還是向火竜開始了猛攻。
在那個空檔艾夏提煉了夜色的魔力。

「對不起勉強你，再借我一次力量，溫蒂妮！」

艾夏幹勁十足地那樣告知，可是──

「啊──抱歉，那辦不到──」

因為意料之外的回答思考停止了。

「欸⋯⋯？」
「哎呀─，我們精靈是選擇魔力的種族啦──所以，那個，歪曲的魔力用不了哦，嗯⋯⋯」

艾夏的魔力現在被龍之從者拉攏，和娜哈特一樣變成了龍的。
因此，早就脫離了精靈能使用的領域。

「那，那我，該怎麼辦──」
「誰知道呢？最好是逃跑吧──就算不是分裂體，對手是竜種的話我也還是覺得形勢不利呢─。啊，抱歉呢，契約者小姐。時間到了所以我差不多該回去了──」

那樣說後，水形象化的人消失了，水滴落下了。

「呋欸欸欸欸欸欸欸欸欸欸欸欸欸欸欸欸欸欸欸欸欸欸欸欸欸欸欸欸！！那我該怎麼辦──」

克麗絲塔小姐，公會長先生──想要那樣出聲但閉口了。
火竜的目標只有艾夏。那樣的話，不希望又把他們捲進來，造成受害。
聲音傳到了困惑的艾夏那。

「啊─，真是，相當痛啊這槍──刺進爪子間了！不過嘛，戲法這樣就結束了嗎？」

竜的牙刺入了惡魔，另一只也被粉碎了頭部倒下了。
好不容易爭取的時間艾夏也浪費了。狀況可以說是最糟。
沒有什麼，沒有什麼，嗎。
艾夏的戰術。
可以不死的方法。
雖然拚命尋找可是找不到。
儘管魔力有的是，卻無法利用那個。

「沒了呢。那麼，請去死吧──」

艾夏哪怕是被一顆牙扎入都會簡單地絶命吧。那樣的強大的，恐怖的象徵的嘴張開了，牙齒迫近了。

腦海中浮現的是跑馬燈。
與父親的生活。
二十年的回憶。
在此之上，僅僅一個月都沒滿的與娜哈特的生活縈繞在腦海中。

討厭。

在那一天在森林中彷徨瀕死時之上的，無論如何都討厭死。
再一會兒，可以的話想一直在娜哈特的身邊。
可是，牙齒毫不留情地刺入艾夏，代替肚子裂開，從娜哈特那領取的手鐲彈飛了。

那之後不記得了。
感覺身體無意識地動了。像靈魂出竅一樣飛了出去，在加速的意識中身體裡有別的什麼動了，那樣的感覺。
和平時不同，東西看得很清楚。世界緩慢地流逝，慢得連竜的牙齒數都能數清。
艾夏沒有發覺自己的眼睛裡隱約出現了金色的圓環。
只是無意識地──將右手聚集的漆黑的魔力扔向了竜。

「嗯嘎啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊！」

閃光加爆發。
雖說是爆發但並沒有起火，應該說是黑色的波動擴展了吧。衝擊波猛烈地，儘管在空中破裂了，還是挖去了赤熱的地面。
不知是不是被莫大的推進力推著，巨軀在空中飛舞。

「好痛─！混蛋！你好大的膽子呢！咱已經生氣了！這次不會手下留情了！」

竜第一次發出了悲鳴。
給予了明確的傷害。
但是，那或許是反效果。
感到憤怒的火竜再次張開口，打算發射那個創造出地獄的吐息（Breath）

「我⋯⋯做了什麼⋯⋯⋯⋯可是，好像已經，不行了──」

身體動不了。失去身體裡的魔力後感到了暈眩，感覺意識要模糊了。已經，一步也不想動了。

對不起，娜哈特大人。

永別了。

一滴眼淚順著艾夏的臉頰流下──

如太陽般收束的火炎的塊吞沒一切回歸於無，本該如此的。

「消失吧，原初的深暗（Darkness）」

充滿了憤怒的音調傳來。
那樣的聲音。
艾夏最喜歡的那個聲音，

──在淚水落到地上前，確實地在耳邊回響了。