周四放學後，下午兩點五十分。

春雪朝著某處幾乎整個一年級國中生活都不曾踏入的區域快步行進。

梅鄉國中的校舍年代相當久遠，一般教室與專科教室的校舍相互平行，中間則由運動校舍連接，形成一個H字形。

而這個H字形中間的橫杠部分裡頭，跟用來舉辦入學典禮的體育館相鄰的武道館，就是春雪眼下的目的地。他當然不是準備加入柔道社來活用自己的體重，如果有教導學生射擊或武術的「特種部隊社」，他倒是想加入，但很遺憾地並沒有這樣的社團存在。

真要說起來，今天的主角應該是拓武，而不是根本沒打算加入任何社團的春雪。

一接近道館的入口，便聽見裡頭傳出壓低的加油聲，以及大得壓過觀眾聲音的清脆打擊聲響。春雪脫下外出鞋放進自己的袋子裡，踏上擦得十分乾淨的木質地板。

他從不算太多的參觀者所圍成的圈子裡，找到了一個眼熟的短髮後腦勺，小跑步接近對方。千百合回過頭來，瞬間噘起了嘴小聲抱怨：

「小春，你來太慢啦！小拓都已經比完一場了。」

「不好意思啦，反正第一輪的對手也只會被阿拓瞬殺吧？」

「話是這麼說沒錯啦。」

春雪將目光從千百合鼓起的臉頰上移開，踮起腳尖環顧四周，馬上就從並排坐在比賽場地另一頭那群身穿護具的劍道社社員之中，發現了舉止顯得特別自在的兒時玩伴。看來他也同時發現了春雪，右手做個小小的動作朝他打招呼。

春雪輕輕點頭回應，注意力轉回到比賽場地中。

「嘿啊啊啊啊！」

「喝啊啊啊啊！」

兩名小個子的社員正以竹刀互劈得十分熱絡。從尖銳的喊聲跟綠色的垂繩來看，雙方多半都是一年級新生。

今天舉辦的是梅鄉國中劍道社全社參加的錦標賽。儘管有著要分出主力跟准主力社員的名目，但聽說同時還有另一個目的，就是讓高年級生對新進社員來個下馬威。梅鄉國中劍道社由於有專用的道場，長年來維持著一定的實力，像今年就有十名左右的社員加入，其中唯一一個二年級新進社員就是拓武。

拓武本人從今年年初轉校過來的那天起，就打算將所有時間都奉獻給黑暗星雲，但黑雪公主則大力反對。拓武一向把這位「軍團長」主張的「不要把現實生活之中的一切都跟BRAIN BURST同化」奉為圭臬。而據她所言，拓武應該也有心繼續練習浸淫已久的劍道，到了今年春天，拓武才總算提出了入社申請。

按照春雪的解釋，拓武之所以邀春雪跟千百合來看這場錦標賽，多半是想向他們表明心態，告訴他們說即使會輸掉比賽，也絕對不會在把「加速」用在劍道上。就是因為這樣，春雪才會來到一直不太敢踏進的運動社團地盤。

「胴！勝負已定！」

社團指導老師的喊聲響起，打斷了春雪的思緒。

其中一名一年級生退回開始線後方，收起竹刀後仍然掩飾不住懊惱，踩著重重的腳步聲忿忿不平地入列，相較之下，獲勝的一方則輕飄飄地轉過嬌小的身軀，無聲地退回場外。

春雪眯起眼睛目送他人列，但指導老師的喊聲打斷了他的視線。

「第二輪，第一場比賽！紅方，高木。白方，黛！」

兩名學生應聲站起。高木是三年級生，黛——拓武當然是二年級。兩人的身高差不多，但高木的身材顯然厚實得多。

拓武敬禮後走上三步，在開始線上蹲踞，竹刀一動也不動地定在中段。春雪則在一旁凝視著過程。

仔細想想，這還是他第一次用肉眼看到拓武穿劍道服的樣子。當然他曾經看過上傳到網路上的比賽影片，但現場的資訊量終究不一樣。不但感受得到用了許久而發出黑色光澤的竹刀份量、道服堅硬的質地，甚至連護具的味道都彷佛傳了過來，讓春雪不由得吞口水。

這足足維持了一百年以上的劍道選手裝扮之中，唯一的異質光輝，就是頭巾下微微露出的神經連結裝置。

運動比賽採用這種裝置的主要目的，是在於以覆蓋視野的方式顯示雙方得分與比賽時間，另外還有用在劍道或西洋劍等比賽的有效打擊判定。只要利用神經連結裝置的戚覺回饋功能，就連差距只有數百分之一秒也不稀奇的打擊先後順序，都照樣可以輕易判定出來。

當然比賽中對於選手是否執行外掛程式及連上全球網路，都會進行嚴格檢查，然而這世上卻有種超越這一切的程式，可以輕易鑽過這些監視，那就是「BRAIZ BURST」。

拓武在去年夏天進行的東京都國中校際劍道大賽中，就有動用「加速」能力，所以還只是一年級就奪得了冠軍。然而他也因此消耗了太多超頻點數，瀕臨喪失BRAIN BURST程式的危機，走投無路之際，甚至在千百合的神經連結裝置中植入病毒，企圖將黑雪公主——「Black Louts」的點數搶奪殆盡。

對於這些所作所為的後悔，直到千百合與黑雪公主都已經原諒他的現在，仍然強烈留在拓武心中。

然而春雪可以感覺出來，拓武現在重新回到劍道場上，正代表他試圖踏出新的一步。

「小拓——！一劍劈了他——！」

身旁千百合毫不客氣的聲援，讓春雪忍不住縮起脖子，但他隨即也卯足全力大喊：

「阿、阿拓！加油！」

面對三年級的高木，拓武雖然被擊中一次，但仍然獲得勝利。

接下來的准準決賽他也順利得勝，準決賽雖然拖到要比得分，但仍然獲得勝利，挺進到了決賽。

然而集本次錦標賽話題於一身的卻不是拓武，而是位每一輪比賽都直落二取勝，展現驚人實力的一年級新生。

「小、小手！勝負已分！」

指導老師有些破音的喊聲，跟盛大的交頭接耳聲融成一片。「有個一年級超猛的」這樣的傳聞兩三下就經由校內網路傳開，儘管已經過了放學時間，仍然在短短十分鐘之內就涌入了大群學生，擠得比賽場周邊水泄不通。

這個對此顯得完全不放在心上，踩著平順步伐回到開始線上的一年級生，就是春雪所看到的第一場比賽中那位小個子選手，刺在垂掛名牌上的名字寫著能美兩字。

他的身高應該頂多只有一百五十五公分，身材也很瘦，對上大個子的高年級生時簡直就像小孩對上大人，怎麼看都不覺得有辦法打出像樣的比賽。

然而就是沒有人可以擊中他。三年級生的攻擊快得讓春雪甚至沒辦法看清楚，但他卻以彷佛早已料到對方劍路似的反應速度輕巧地閃過，再不然就是看準對方出招之際出手。

照春雪模模糊糊的認知，劍道這種競技除非抓準對方出招或收招之際，不然實在很難有效擊中對手。而前者稱為先之先，後者稱為後之先，也就是說一切的關鍵就在於能以多快的速度去針對敵人的攻擊做出反應。

看樣子這個姓能美的一年級生，在這方面有著極為突出的能力。

沒錯——是「能力」。

「決賽！紅方，能美！白方，黛！」

在指導老師的宣布下，能美與拓武進入了比賽場地，觀眾登時歡聲雷動。

以國中生來說身材相當高的拓武，對上怎麼看都只像國小生的能美，兩人之間有著將近二十公分的身高差距，不用想也知道是拓武比較有利，因為揮刀的距離完全不一樣。然而先前能美對上比自己高大的對手時，卻沒有一次被擊中，每一場都贏得非常徹底。

兩人互相鞠躬，在開始線上采蹲踞姿勢拿著竹刀擺出架勢後，大群觀眾似乎也察覺到了不尋常的氣息，立刻變得鴉雀無聲。看在春雪眼裡，甚至覺得兩人對峙的劍尖之間進出了泛青色的火花。

「開始！」

——老師才剛喊完，兩聲叫喊與一聲打擊聲就在寬廣的劍道場上交錯。

先動的人是拓武，至少看在春雪眼裡是這樣。他在站起的同時跨步上前，緊接著就大喝一聲：「面——！」揮刀直劈。這從剛好構得著的間距看準對方顏面出手的一擊毫不容情，攻擊距離較短的能美根本無法反擊。照理說是這樣。

然而拓武的竹刀還沒揮到底。

「手——！」

能美的竹刀就在這一瞬間的呼暍聲中擊中拓武手腕。啪一聲，清脆的打擊聲響重重撅動了空氣。

拓武追上前想要造成雙刀互擊的態勢，但這時能美已經拉出足夠的距離，高高舉起了竹刀。

「小手！」

紅旗隨著這一喊舉起，這時觀眾與三十名以上的社員才大聲鼓噪。就連站在春雪身旁的千百合也瞪大了眼睛大喊：「不會吧！」

春雪心中也只覺得不敢相信。

先動的人是拓武，這點絕對錯不了。

而拓武是從自己攻擊範圍剛好構得著的距離瞄準對方的顏面攻擊，但招式出到一半就被擊中手腕，這代表著什麼呢？代表能美完全掌握住了拓武出招的軌道與時撥，並根據預測「搶先放好」了自己的竹刀——理論上不就是這麼回事嗎？這不是先之先，也不是後之先，說來算是「中之先」。

春雪驚訝得甚王忘了眨眼，一瞬間還懷疑起這裡到底是現實世界還是虛擬世界。

如果是在虛擬世界——在反應完全取決於大腦回應速度的電子世界之中，這樣的反應或許真的有辦法辦到。然而在現實世界之中，任何動作都會受到好幾種物理定律盯限制。考慮到會將沉重的身體留在原處的慣性、神經的傳導速度、肌肉的收縮速度等因素，要在看到對方出招之後才搶在前頭揮劍擊中對手，根本是不可能的，絕對不可能。

只有一種特定人士才具備的「能力」例外。

春雪感覺到握緊的雙手都被滲出的汗水弄濕，兩眼注視著再度回到開始線上對峙的雙方。

「第二戰！」

這次的情形跟第一戰時完全相反，拓武握著竹刀一動也不動，保持跟對手之間的距離。面罩下的雙眼有如刀刃般銳利，嘴唇緊閉。

相較之下，能美的劍尖則輕飄飄地上上下下，絲毫感受不到緊張的氣息。逆光讓人看不清楚他的臉，但看在春雪眼裡卻覺得他的嘴唇上有種輕薄的笑容。

十秒。二十秒。

雙方都不出招，唯有時間不斷流動。

春雪的雙眼瞪得不能再大，持續投注全副精神在能美的臉上。

如果春雪的推測——或說是不祥的預感——正確，能美將會在某個時間點上開口，為的是以沒有任何人聽得見的音量，念出一個簡短的語音指令。

雙方都只沿著順時針方向緩緩繞行，最後指導老師終於深深吸一口氣。

但就在他即將喊出「時間到」之際——

能美忽然以看起來不怎麼快的速度揮起了竹刀。

而春雪也終於看見了。看見能美的嘴迅速地小幅度開閉。

——錯不了。

是加速指令。

這個姓能美的一年級新生，是個脖子上的神經連結裝置中有著神秘超級應用程式「BRAIN BURST」的超頻連線者。

「胴！」

就在能美舉起竹刀的瞬間，拓武朝著他空門大開的內側劈去。

同時春雪也在口中低喊：

「超頻連線！」

隨著啪一聲音效響起，世界凍成了一片藍色。

即使透過加速到一千倍的知覺，仍然可以看到拓武的竹刀正一厘一厘地逼近能美的軀幹，怎麼看都覺得能美應該已經沒有任何手段可以閃避或格擋這一刀。沒錯，就算他已經在這一瞬間啟動「加速」也不例外。

春雪以粉紅豬虛擬角色的模樣進入比賽場地內，探頭往能美的面罩內看去。很遺憾的公共攝影機並沒有包括到面罩內的部分，看不清楚他的長相，唯有露出微笑的嘴角透過多邊形重新建構出來。

春雪瞪著他的臉，以左手啟動BRAIN BURST的操作介面。

春雪不清楚這個姓能美的一年級新生是如何躲過剛入學時自己跟黑雪公主的檢查，但現在既然有在參加比賽，照理說能美的神經連結裝置就一定要連上梅鄉國中的校內網路，那麼他的名字也就非得出現在對戰名單中不可。

——我就抓準這一瞬間找你「對戰」。

春雪如此下定決心，等候名單更新。能美顯然已經在劍道社的比賽中動用加速能力，那麼緊接著在下周舉辦的學力測驗，他想必也打算這麼做。然而有件事他非得告訴梅鄉國中的超頻連線者不可，那就是必須遵守黑暗星雲軍團的鐵則：「在考試或比賽中不可動用『加速』。」如果有需要，甚至不惜動用虛擬的拳頭。

在搜尋字樣消失後，名單上接連出現了Silver Crow、Black Lotus、Cyan Pile．以及Lime Bell的名字。

「咦……？」

春雪劇烈地喘著氣，伸向名單的右手僵住不動。

沒有。

名字沒有出現。就跟前幾天一樣，名單上只有四名已知的超頻連線者名字。

「為……什麼？」

他茫然地喃喃自語。

怎麼想都不覺得這是誤會。相信拓武也看準了能美是超頻連線者，所以第一戰時才會絲毫不觀望就上前攻擊，為的自然是不給對方空檔念出加速指令。

難道說半年前拓武用的後門程式又流出來了？春雪瞬問想到這個可能性，但立刻拋開。那個程式是用別人來當跳板，讓封閉式網路外的人可以跟裡頭連線。

然而現在這一瞬間，能美確實待在梅鄉國中的劍道場上，那麼他無疑應該有跟校內網路連線，所以他非得出現在對戰名單上不可，絕對沒有例外。

春雪那豬型虛擬角色短短的雙手抱在胸前，低頭拚命運轉思考回路。他整整花了一分鐘，才整理出三個可以說明這個狀況的可能性。

一、能美不是超頻連線者，純粹是劍道的天才。

二、能美是超頻連線者，但沒有連上校內網路。

三、能美是超頻連線者，也有連上校內網路，但可以拒絕出現在對戰名單上。

相信真相一定就在這些可能性之中，但仍然有無論如何都難以說明的部分。

春雪承受著這種難以言喻的不痛快戚，呼出一口長氣。現在多想也沒用，只能等事後再找拓武商量看看了。

春雪回到自己那變成藍色而靜止不動的身體上，又朝能美的身影瞪了一眼。

能美或許是有勇無謀地想擊向拓武的顏面，他正高舉竹刀想要撲過去砍劈。拓武針對這個動作揮刀掃向能美的軀幹，時機的完美就連春雪這個外行人都看得出來。

就算能美是超頻連線者，是劍道的天才，又或者兼具這兩種條件，終究還是無法可想。春雪心想至少要用肉眼見證到拓武拿下一場的模樣，於是瞪大眼睛念出了停止加速的指令。

「超頻登出！」

現實中的聲響從遠方慢慢接近，同時藍色的世界也逐漸找回原有的顏色。拓武與能美的動作正慢慢地，慢慢地恢復到原有的速度——

這時春雪又遭逢了短短幾分鐘內已經不知道第幾次的驚愕。

能美的身體往右偏開了。

那不是挪步之類的動作就能造成的偏栘，能美只有左腳腳尖碰到比賽場地的地板，然而他卻以這唯一的一點為軸心，矮小的身體就像在花式滑冰似地，一邊往左旋轉一邊往右偏栘。拓武的竹刀逼近，但軀幹護具的外皮卻越逃越遠……

春雪的知覺加速就在這時完全解除。

霹的一聲輕響，拓武竹刀刀頭的皮套打在了能美的軀幹護具上，但角度卻很淺。

緊接著能美肆意舒展的竹刀準確地命中了拓武的面罩。

右腳咚一聲踹在地板上，順勢拖刀斬過。

「面！」

一聲大暍之後，連後勢的動作都無可挑剔。

過了一會兒，鴉雀無聲的劍道場上響起了「擊中顏面！」的喊聲。

「……勝負已分！」

裝著鞋子的提袋咚一聲從春雪手中落下。
