傭兵都市貝爾菲因。

伽羅亞瑪利亞附近的大規模城市，在周邊城市国家群中軍力尤為突出。街上充斥著傭兵、冒険者，那些接近崩潰的人們，每天都沉浸在享樂中，過著不知明日的生活。

為什麼貝爾菲因擁有僱傭兵或者冒険者，甚至城市名也與其相關？這和貝爾菲因的政策也有關，但比什麼都重要的是其地理條件。

伽羅亞瑪利亞是一座繁榮昌盛的貿易都市，當然有大量的商人和物品流入。物品流入後人群就會聚集，人群聚集的話總有一天會惹來山賊和竊賊。
因此商人追求武力，為了不讓自己的財產被奪走，而追求作為防衛的武力。

當然，在伽羅亞瑪利亞也有傭兵、冒険者之類的人存在。但是，那並不是訪問伽羅亞瑪利亞的所有商人都能請動的。
雖然伽羅亞瑪利亞發展迅速，但卻被堅牆環繞著，所以人口容量相當有限，而有限的護衛顯然無法滿足不斷膨脹的經濟。

因此，傭兵都市貝爾菲因成為了商人和伽羅亞瑪利亞之間矛盾的緩衝。貝爾菲因大量接納傭兵，成為無根草的商人們的後盾。
傭兵們上繳收益的一部分，而取得合法地位。於是，就像冒険者得到貴族作為後盾一樣，在貝爾菲因，都市国家本身就成了傭兵的盾牌。

畢竟貿易都市就在附近，所以對傭兵的需求非常龐大，需要護衛的商人全都投身於貝爾菲因。

大體上就是經過了那樣的過程，然後傭兵都市貝爾菲恩就確立了如今的地位。

現在，像這樣走在眼前的大街上的，很多都是快步奔跑的商人和目光凶惡的傭兵們。我外出也沒什麼不方便的。
把馬寄存好，踩在街道的沙子上。總覺得，很令人懷念啊。酒、砂和血混合在一起的奇妙氣味。還有從烤肉的攤子上散發出的焦臭的味道。

感覺不錯，誰都不在這裡，只有我一個人。感覺很奇妙，肩膀都變得輕盈了。

雖然很任性，但是周圍的人總是太多的話，偶爾會想自己一個人獨處。回想起來，過去的救世主，赫爾特・斯坦利也有這樣的時候嗎？那傢伙在那次旅行中經常和別人同行，真是了不起的精神。

不管怎麼說，在這裡，住哪個酒館，接觸怎樣的人，使用怎樣的手段，全部都取決於我。真是輕鬆無比。沒有必要一一去看聖女大人、卡麗婭、芙拉朵的臉色。
邁著輕快的步伐，穿過擁擠的街道。

不管人再多，要走熟識的路，也不算苦難。我過去在這裡丟過一個錢包，現在在這裡走著，總覺得有點怪怪的。總感覺會重蹈覆轍，手縮成一團。

就那樣我慢悠悠地在街道上晃悠，突然，像消失一樣地側進了小道。


◇◆◇◆

「嗯⋯⋯這個名字的傭兵不在我們酒館裡。」

酒館的老板一邊撫摸著夾雜著白色的鬍子，一邊冷靜地說道。這雙眼睛之所以看起來迷迷糊糊的，是因為現在還是白天吧。
但是，這也太奇怪了。這裡應該是那傢伙常來的酒館。

「請再好好想一想吧。一定會在這裡，他是一個喜歡低品質朗姆酒的人。」

老板皺起眉頭，「不知道啊」，再次張開嘴，連下巴都沒動。怎麼也不覺得那是認真的回答。不由得嘆了一口氣。
看樣子這老板也沒有發生變化啊⋯

「一個人，啤酒，再配上好的奶酪。那樣就可以了。」

每當這種時候，老板總是帶著愉快地微笑向陶器裡倒入啤酒。其特徵的臉上刻滿的皺紋，一下子全部浮現出來。

但是那個表情，在某處帶有陰影。

「但是⋯⋯還是不知道，那個名字的傭兵⋯無論是不是傭兵，我根本沒聽說過那個名字。」

難道是別的酒吧嗎⋯這次不是鬧別扭的口氣，而是很清楚地說著，那不是欺騙的話語，而是認真地回憶後仍然沒有發現的話語。

果然，很奇怪啊。再次確認店裡的氛圍和從窗戶看到的風景。幾乎沒有整理過的堆得高高的木桶山，透過窗戶看到的賭場的燈光。和記憶中的樣子完全一致。
我想，那傢伙一定就是在這個酒館裡用啤酒浸潤舌頭。我皺起了眉。

這裡，傭兵都市貝爾菲因，以前我曾一度作為據點的地方。
畢竟這裡有很多工作。我那時一邊疏浚溝道一邊作為冒険者進行活動才得以生存下來。對於我來說已經是不錯的光景了。

但是，儘管如此，絶不能說只有好的回憶。不如說，殘酷的記憶記得更清楚。不知不覺中嘴唇扭曲了。
在貝爾菲因逗留的時候，雖說只是暫時的，但確實聯手過的男人，應該就在這間酒館。我撫摸著下巴，眯起眼睛。

時間稍微偏離了嗎？我已經不記得第一次見面是什麼時候了，或許現階段他是以其他酒館作為據點？

沒辦法了。

咬著變硬的奶酪，強行咽下啤酒使之滑入喉嚨。苦味和難以言喻的酸爽讓心情變得更加舒暢。
真沒想到，居然還會再在這片土地上這樣飲酒。明明再也不想踏上這片土地了。奇怪的是，心情還挺好的。

那是高漲的情緒，還是不沉著地浮躁著，我也不明白。

「──老板。處理一下每天在店前討酒的醉漢吧，會降低店舖的評價的。」

剛把第二杯啤酒放在嘴上的時候，進店的傭兵心情愉悅地開口說到。
我很清楚這些話意味著什麼。也很清楚他下一句話會說什麼。

「今天我已經收拾了。接下來的話⋯⋯」

這是空有一身蠻力的傢伙經常做的事。
踢鳥和乞丐，為了消除郁憤而踐踏他們。實在是無聊，似乎是覺得這樣做能讓自己變得了不起。

但那也不是什麼值得指責的行為，況且我也沒有批評他的立場。
因此我毫不介意地繼續向喉嚨灌入啤酒。感受著熱量在食道奔跑。

突然，我猛的起身。在腦海中，有如記憶碎片般的東西通過了。腦海中靈光一閃。

──討酒的醉漢。

不，等一下。那樣的事，不可能。但是，我有一種奇妙的預感，就好像那是確信的事實一樣，

我把銀幣丟在桌子上，就這樣從後門出去。
發生糾紛時，沒有人會把大馬路當做舞台。因為那樣做的話，官員就不能視而不見了。

因此，無論何時，爭吵和打架都是在後街，這是固定的場所。生活垃圾的味道和泥土的味道混雜在一起非常刺鼻。我一邊避開人們丟棄的髒東西，一邊把視線投向後路的深處。

在那裡有一道人影。大概是被狠狠地打了吧，靠在牆壁上，手腳呈大字型。

軀體的線條不那麼粗獷，反而很細。戴著寬檐帽子的模樣，垂著頭，有些淤青的嘴唇掛在他的臉上。──特徵與我熟悉的人一樣。
一邊想著不會吧，一邊張開嘴唇。臉上不知不覺中露出了笑容。
內心深處。在搖曳的感情中，不知為何有著不可思議的信心。

「──布魯達，你在幹什麼呢？怎麼一副要死了的樣子。」

一邊縮著肩膀，一邊輕聲地對著老朋友說著。不過啊，雖然我認識對方，對方對我倒是一點都不了解。
那傢伙，和以前完全沒有變化，用熟悉的聲音說到。

「好的，你知道我的名字。但是很遺憾，傭兵布魯達已經退役了，現在叫我酒鬼就行了。這樣一來，頑固的酒館老板就會把酒分給我吧！」

聽起來覺得愉快的那種聲音，毫無疑問，是我很熟悉的那位傭兵，曾經是我的工作伙伴，然後，

「那就好，可以盡情地喝酒就好了。但是，希望你再一次干回傭兵的本行。」

曾經死過一次的我的朋友，布魯達。