小胖男交出了貼在板子上的紙跟筆。是不需要墨水的筆。

「請在這裡寫上名字和想要進行秘密鑑定的物品數量」

雷肯寫上自己的名字和數字一後交出。男性輕快地寫了些什麼。

「好的。可以了。秘密鑑定一品目要收大金幣一枚。不會製作鑑定書的副本，會把原書交給委託人。收購金額不會記在鑑定書上。能看鑑定書的只有鑑定士本人和委託人。鑑定士會發誓，死前都不會洩漏在秘密鑑定得知的事。我也不會對任何人洩漏在這房間看到的事。以領主大人之名向大神艾雷克斯對此發誓。以上。如果接受，請支付大金幣一枚」

雷肯付了大金幣後，小胖男隨意地將之放入腰袋，拿著文件移動到房間的角落。像是在表示已經沒興趣了，張望著別處。

老鑑定士從桌子的抽屜拿出一張鑑定書。

「好，雷肯。把物品拿出來」

想著，被死老頭給記住名字了，雷肯取出在尼納耶買的〈箱〉，從中拿出〈彗星斬〉放在桌上。
老鑑定士舉起漆黑的細杖，詠唱了準備詠唱後發出發動咒文。

「〈鑑定〉」

然後寫到鑑定書上，交給雷肯。
此時，小胖男的眼睛詭異地閃了一下，但雷肯看漏了。

〈名稱：彗星斬〉
〈品名：魔法劍〉
〈攻擊力：二十〉
〈硬度：二十〉
〈堅韌度：二十〉
〈鋒利度：五十〉
〈消耗度：無〉
〈耐久度：一百〉
〈出現場所：茨波魯特迷宮百二十一階層〉
〈製作者：〉
〈深度：百二十一〉
〈恩寵：魔法刃（攻擊力十倍、鋒利度十倍、長度兩倍～五倍）、破損修復〉
※魔法刃：注入魔力就會生成魔法之刃。長度會根據注入的魔力量。維持魔法刃需要持續供給魔力。
※破損修復：在劍身受到損傷的場合，會自然地修復。

果然是〈彗星斬〉。雷肯邊為感動顫抖，邊注視了鑑定書一陣子後，提出質問。

「這個記述是什麼意思」

雷肯指在〈攻擊力十倍〉的項目上。
老鑑定士把鑑定書拉過去並翻面，輕快地寫上了什麼並還給雷肯。
〈在這把劍的場合，劍身本體具有的攻擊力為二十。會生成具有其十倍的攻擊力的魔法刃。攻擊力的數值越高，發動和維持就需要更多的魔力。但在這把劍的場合，長度最少是兩倍。如果是等倍，長度就沒有差，發動的魔力量就不多，但來到兩倍的長度就需要驚人的魔力量。很遺憾，這把魔法劍，是魔力量非常豐富的魔法使借助了魔力增加或補充的恩寵品，才總算能發動並維持的物品〉
換句話說，這魔法劍的攻擊力是兩百。

兩百。
多麼可怕的攻擊力。

就連〈阿格斯特之劍〉的攻擊力也只有五十九。當然，所謂的劍，並非任何人怎麼揮都有相同的攻擊力。在這〈攻擊力〉加上劍的重量、硬度和鋒利度，使用者的技術和揮出的速度等等，才會是實際的攻擊力。不過，基礎值兩百是很異常的數字吧。

至於鋒利度，基礎值則會是五百。真恐怖的數字。這下就幾乎沒有砍不斷的東西了吧。

（想知道現在使用的〈威力劍〉的數字）
（待會再去一次收購所吧）

「要賣嗎？」

如此搭話的老鑑定士的眼，看起來也像浮現了感到遺憾的表情。

「要賣的話會值很不得了的價格。沒辦法馬上評定吧」

能將這把劍的力量發揮出來的人很少。但如果是這国家的最高峰的魔法劍士，就能發揮出這把劍的力量吧。這把劍讓地方貴族擁有也沒用，但對国家和神殿來說是很難得的強力武器。雷肯對感覺能使用這把劍的對象有頭緒。

神殿騎士，德魯斯坦・瓦爾莫。
那男人，估計用得了這把劍。不知道能用到何種程度的威力跟精度就是了。然後，既然有一個那種男人，那麼国家中央裡存在能匹敵，甚至超越的魔法劍士，也並無不可思議。

国家和神殿，肯定會很想要這把劍吧。

「不。剛剛有試著使用過，我用得了這個。沒有要賣給別人。要自己來用」
「喔」

威嚴的臉龐浮現了少許驚訝。
雷肯故意轉過頭，看了小胖男。在快速翻閱手上的文件，看起來心情好到會哼歌。

（沒有從那位置移動）
（從那邊應該看不到〈鑑定書〉的文字才對）

「還有其他想知道的事嗎」
「沒有。了不起。我沒辦法鑑定這把劍」
「什麼？你是〈鑑定〉技能的持有者嗎？」
「啊啊。到這個的上一階層都還能鑑定就是了」
「鑑定看看吧」
「什麼？」
「現在，在這裡，對這把劍施展〈鑑定〉看看」
「為什麼我得做那種事」
「別管了做就是了」